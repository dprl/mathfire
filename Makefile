all:
	./bin/install

clean-example:
	rm -rf index/

# By default, avoid reloading tangent modules and LaTeXML
clean:
	./bin/clean-save-tangent-latexml

# Rebuild: remove and rebuild everything
rebuild:
	./bin/clean
	./bin/install

# Simple conversion test
example:
	./bin/run	Sample_files/  index


start-resources:
	./bin/start_resources