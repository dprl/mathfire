# Author: Behrooz Mansouri (bm3302@rit.edu)
# The goal of this class to manage the intermediate index files and directories
import csv
import os
from latex_mathml_conversion_utils import ConversionUtils
from IndexFilesControllers.file_manager import FileManager, FileType


class IndexFilesController:
    def __init__(self, source_dir, write_limit):
        """
        This class manages the intermediate index files and their directories. It takes in the sourse (root) directory
        in which subdirectories for index files should be created. Also, each TSV index file has a limit for the number
        of records that can be saved in it.
        @param source_dir: intermediate index source directory
        @param write_limit: TSV index file record limit
        """
        self.__source_directory = source_dir
        self.__formula_location = self.__source_directory + "/FormulaLocation"
        self.__file_names = self.__source_directory + "/FileNames"
        self.__formula_latex = self.__source_directory + "/FormulaLatex"
        self.__formula_pml = self.__source_directory + "/FormulaPML"
        self.__formula_cml = self.__source_directory + "/FormulaCML"
        self.__failure_log = self.__source_directory + "/FailureLog"
        # setting directories and last file ids
        self.last_formula_file_index, self.last_file_path_index = self.__init_directories()

        # creating visual ids dictionaries
        self.visual_ids_slt = self.__read_visual_ids()
        self.visual_ids_latex = self.__read_visual_ids(is_slt=False, slt_visual=self.visual_ids_slt)

        # setting the last visual id and last formula id, these ids will be used to continue indexing
        self.last_visual_id = self.__get_last_visual_id()
        self.last_formula_id = self.__get_last_formula_id()

        # creating file manager for each intermediate index
        self.file_formula_location = FileManager(self.__formula_location + "/", self.last_formula_file_index,
                                                 write_limit, file_type=FileType.location)
        self.file_doc_paths = FileManager(self.__file_names + "/", self.last_file_path_index, write_limit,
                                          file_type=FileType.document)
        self.file_formula_latex = FileManager(self.__formula_latex + "/", self.last_formula_file_index,
                                              write_limit,
                                              file_type=FileType.representation)
        self.file_formula_pml = FileManager(self.__formula_pml + "/", self.last_formula_file_index, write_limit,
                                            file_type=FileType.representation)
        self.file_formula_cml = FileManager(self.__formula_cml + "/", self.last_formula_file_index, write_limit,
                                            file_type=FileType.representation)
        self.file_failure_log = FileManager(self.__failure_log + "/", self.last_formula_file_index, write_limit,
                                            file_type=FileType.errors)

    def __init_directories(self):
        """
        This method check and manage the directories for the intermediate and returns the last file ids
        @return:
        """
        last_formula_file_index = 0
        last_file_path_index = 0
        """
        If the main directory that keeps all the intermediate directories for TSV file is not created, create it and
        create the sub-directories
        """
        if not os.path.isdir(self.__source_directory):
            os.mkdir(self.__source_directory)
            os.mkdir(self.__formula_location)
            os.mkdir(self.__file_names)
            os.mkdir(self.__formula_latex)
            os.mkdir(self.__formula_pml)
            os.mkdir(self.__formula_cml)
            os.mkdir(self.__failure_log)

        else:
            "If the sub-directories exists get the last id of formula index and document index"
            for file in os.listdir(self.__formula_location):
                if int(file.split(".tsv")[0]) > last_formula_file_index:
                    last_formula_file_index = int(file.split(".tsv")[0])
            for file in os.listdir(self.__file_names):
                # The id for the document file path index can be different from formula files index
                if int(file.split(".tsv")[0]) > last_file_path_index:
                    last_file_path_index = int(file.split(".tsv")[0])
        return last_formula_file_index + 1, last_file_path_index + 1

    def __get_last_visual_id(self):
        """
        @return: the visual id that will be assigned to a new visually distinct formula being indexed
        """
        if len(self.visual_ids_slt.values()) > 0 and len(self.visual_ids_latex.values()) > 0:
            return max(max(self.visual_ids_latex.values()), max(self.visual_ids_slt.values())) + 1
        elif len(self.visual_ids_slt.values()) > 0:
            return max(self.visual_ids_slt.values()) + 1
        elif len(self.visual_ids_latex.values()) > 0:
            return max(self.visual_ids_latex.values()) + 1
        return 1

    def __get_last_formula_id(self):
        """
        @return: the formula id that will be assigned to next formula being indexed
        """
        formula_id_max = 0
        dir_path = self.__formula_latex + "/"
        for file_path in os.listdir(dir_path):
            temp_list = []
            with open(dir_path + file_path, mode='r', encoding="utf-8") as infile:
                csv_reader = csv.reader(infile, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                next(csv_reader)
                for row in csv_reader:
                    formula_id = int(row[0])
                    temp_list.append(formula_id)
                if len(temp_list) > 0:
                    temp_max = max(temp_list)
                    if temp_max > formula_id_max:
                        formula_id_max = temp_max
        return formula_id_max + 1

    def __read_visual_ids(self, is_slt=True, slt_visual=None):
        """
        Read the visual ids in dictionaries. Note that this method is for both SLT and LaTex visual ids.
        If the LaTex index is being read, then it should check if the visual id is not read by SLT index. The LaTex
        visual ids are only for those formulas that SLT does not exist.
        @param is_slt: determines if the SLT index is being read
        @param slt_visual: if the LaTex index is being read, then the dictionary of SLT visual ids should be passed
        to this method and the method checks to not include the LaTex visual ids that are already in SLT index
        @return: a dictionary of {representation (slt or latex): visual id}
        """
        temp_visual_ids = {}
        dir_path = self.__formula_pml + "/"
        for file_path in os.listdir(dir_path):
            with open(dir_path + file_path, mode='r', encoding="utf-8") as infile:
                csv_reader = csv.reader(infile, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                next(csv_reader)
                for row in csv_reader:
                    visual_id = int(row[1])
                    presentation = row[2]
                    if is_slt:
                        try:
                            string_value = ConversionUtils.convert_mathml_slt_string(presentation)
                        except:
                            continue
                    else:
                        "if the visual id is not in SLT visual ids, then the LaTex representation is used"
                        if visual_id in slt_visual.values():
                            continue
                        # a + b --> a+b
                        string_value = "".join(presentation.split())
                    temp_visual_ids[string_value] = visual_id
        return temp_visual_ids
