import os
import csv


def read_arqmath_tsv(mathml_dir, destination_dir):
    for file in os.listdir(mathml_dir):
        with open(file, mode='r') as infile:
            csv_reader = csv.reader(infile, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            next(csv_reader)
            for row in csv_reader:
                formula_id = row[0]
                representation = row[5]
                visual_id = row[4]