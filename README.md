# MathFIRE <br/>(Math Formula Indexing and Retrieval for ElasticSearch)

**Authors:** Behrooz Mansouri, Matt Langsenkamp, Richard Zanibbi   
[Document and Pattern Recognition Lab (dprl)](https://www.cs.rit.edu/~dprl/index.html), Rochester Institute of Technology, USA  

MathFIRE is part of the [MathSeer](https://www.cs.rit.edu/~dprl/mathseer) project.

## Version 0.1.0 (May 2022)

**MathFIRE** is a framework for indexing and retriving formulas extracted using the [MathSeer Extraction pipeline](https://gitlab.com/dprl/MathSeer-extraction-pipeline). Extracted information includes document identifiers, 
formula locations, and formula representations in LaTeX, Presentation MathML (representing appearance), and Content MathML (representing formula operations, i.e., operation syntax). 

Using this extracted information, MathFIRE is able to produce formula embeddings (vector representations) using [Tangent-CFT](https://gitlab.com/dprl/TangentCFT). Tangent-CFT uses [fastText](https://fasttext.cc) n-gram embeddings to produce four vector representations for formulas: Symbol Layout Tree (SLT) and Operator Tree (OPT) vectors/embeddings for the concrete formulas, and SLT-TYPE and OPT-TYPE vectors/embeddings where arguments (e.g., variables and numbers) are replaced by their types to support unification.

After formula vectors have been produced, MathFIRE uses [OpenSearch](https://opensearch.org) (closely related to [ElasticSearch](https://www.elastic.co)) to index and retrieve formulas in these different representations.


**Formula Visual Identifiers.** Formula index files include a visual identifier for each formula (shown as `VisualId` in sections below). A visual id is shared by formulas with identical appearance. These visual ids are computed based on identical Symbol Layout Trees produced by the [Tangent-S](https://gitlab.com/dprl/tangent-s) system. Unique LaTeX Strings are used to create visual ids where Tangent-S is unable to produce an SLT.


## Installation

### Requirements
1. Python 3.6 or above
2. lxml-4.7.1
3. conda
4. beautifulsoup4-4.10.0
5. pypandoc-1.7.2
6. LaTeXML-0.8.5
7. tqdm
8. [Tangent-S](https://gitlab.com/dprl/tangent-s)
9. [Tangent-CFT](https://gitlab.com/dprl/TangentCFT)
10. [OpenSearch](https://opensearch.org)

### Installation and Test Example 

1. Run `make` to install the requirements in conda environment named **mathfire**. This includes four fastText models (neural networks) for creating Tangent-CFT embeddings.
2. Run `make example` to run sample a conversion command. This will produce formula data files in the `index/` directory (details are provided below).

### Cleaning Up

* To remove the index produced by `make example`, issue `make clean-example`. 

* Run `make clean` to clean the repository, but NOT remove LaTeXML and Tangent-CFT, which take awhile to download and build.

* Run `make rebuild` to fully clean the repository (conda environment + Tangent-S + Tangent-CFT + LaTeXML + default index directory).  This command is useful if you want to rebuild the system from scratch. 


## Converting MathSeer TSV Extraction Files 

Running `make example` reads in a single MathSeer extraction data file `Samples_files/1.TSV`, in Tab Separated Value (TSV) format. It then generates 
five TSV files for (1) file names, (2) formula LaTex, (3) formula Presentation MathML, (4) formula Content MathML, and (5) formula locations (i.e., bounding box locations on pages), along with (6) a TSV file to capture formula conversion errors.

Note that for `make example`, the command actually run is:

```
./bin/run Sample_files/ index
```

where the first argument `Sample_files` is the directory containing TSVs produced by the MathSeer Extraction pipeline,
and the second argument (`index`) is where TSV files for formula data are stored.


**Conversion Outputs:** After running the example, the converted formula data (and error logs) are stored in the directories below.  

1. `index/FailureLog/`: Contains files listing formulas that were not successfully converted, one per input MathSeer TSV file.  Each row in an output file represents a formula id with an issue, and the type of error:

```
    FormulaId  Type
```

2. `index/FileNames/`: Showing document ids and the document file location (file path) on the system. Each output TSV file row represents a pair: 

```
    DocumentId  FilePath
```

3. `index/FormulaLocation/`: Formula location data. Each output TSV file row represents the formula, document, page, and formula location identifiers, along with the bounding box coordinates (MinX, MinY, MaxX, MaxY) for the formula on the indicated page:

```
    FormulaId  DocumentId  PageId  FormulaRegionId  VisualId  MinX  MinY  MaxX  MaxY
```

4. `index/FormulaCML/`:  Content MathML formula encodings. Each output TSV file row contains (`Representation` is a Content MathML formula string):

```
    FormulaId  VisualId  Representation
```

5. `index/FormulaLatex/`:  LaTeX formula encodings. Each output TSV file row contains (`Representation` is a LaTeX string):

```
    FormulaId  VisualId  Representation
```
  
6. `index/FormulaPML/`: Presentation MathML formula encodings. Similar to the two previous formula index files, but `Representation` is now a Presentation MathML string. Each output TSV file row contains:

```
    FormulaId  VisualId  Representation
```


## Indexing and Retrieval with OpenSearch 

In this Section we describe how to perform indexing and retrieval for formula search using the provided example data.  

The main steps for generating and then using an OpenSearch formula index for retrieval are:

1. Generate Tangent-CFT vectors from converted formula TSV files  
2. Instantiate an OpenSearch service instance
3. Load formula vectors into OpenSearch  
4. Issue formula queries to OpenSearch

Details for each of these steps can be found below.

#### 1. Generate Tangent-CFT Vectors

To generate the four different Tangent-CFT formula representations for our example data (i.e., SLT, OPT, SLT-TYPE, and OPT-TYPE representations; described at top of this document), issue the following command:

```
./bin/extract_vectors index cft_vectors
```

where the `index` is the root directory where the converted formula data is located, and `cft_vectors` is the root directory for the Tangent-CFT formula vectors.

This will produce one  TSV file per encoding (with embedding vectors as strings in the final column or each row). There will be four subdirectores of `cft_vectors` named `OPT`, `OPTType`, `SLT`, and `SLTType`.

**Performance Note:**
To extract the four representations in parallel rather than in sequence, you can run the command `./bin/extract_vectors_parallel` instead, using the same arguments.


#### 2. Instantiate OpenSearch Service

**IMPORTANT NOTE:** this step requires sudo access if you are not in the docker user group. 

Run the following command to start Opensearch with docker

```
make start-resources
```


#### 3. Load Formula Vectors into OpenSearch 

**In a new terminal window (separate from the OpenSearch service window that you created in the last step)**, we will now load the CFT formula vectors into an OpenSearch index.


The following command will load the CFT vectors (in TSV files) into an index, with one sub-index for each formula encoding type (OPT, SLT, OPT-TYPE, SLT-TYPE). Note that you need to specify an index name for OpenSearch -- we will use `test_index`. 

Currently, each TSV vector file name needs to be passed separately to OpenSearch. Thankfully, for our example there is just one file/set of formulas to index (1.TSV), and so we issue:

```
./bin/load_in_ES cft_vectors/ "test_index" 1
```
where `cft_vectors/` is the directory where vectors TSV files are located, `test_index` is the index base name in Open Search. The `1` indicates to index the individual file `1.TSV` in each of the subdirectories of `cft_vectors`.


#### 4. Interactive Formula Retrieval using LaTeX 

Let's now run formula retrieval interactively for LaTeX, using the Tangent-CFT2 formula retrieval model ([pdf](http://ceur-ws.org/Vol-2936/paper-04.pdf)).  This model combines all four CFT formula vector representations, combining scores for each representation using a modified reciprocal rank fusion.

Our interactive retrieval command takes in the Open Search base index name, and prints the top-5 results for each input query:

```
./bin/test_retrieval "test_index"
``` 

`test_index` is the name of the OpenSearch base index.  We expect the code for this function to change in the coming weeks and months, but have a look at `./bin/test_retrieval` to get ideas on different ways to run retrieval.

The paper linked to above that describes Tangent-CFT2 is:

```
Mansouri, B., and Oard, D.W. and Zanibbi, R. (2021) DPRL Systems in the 
CLEF 2021 ARQMath Lab: Sentence-BERT for Answer Retrieval, Learning-to-Rank 
for Formula Retrieval. CLEF (Working Notes) 2021: 47-62.
```

## Support

This material is based upon work supported by the National Science Foundation (USA) under Grant No. IIS-1717997, and the Alfred P. Sloan Foundation under Grant No. G-2017-9827.

Any opinions, findings and conclusions or recommendations expressed in this material are those of the author(s) and do not necessarily reflect the views of the National Science Foundation or the Alfred P. Sloan Foundation.
