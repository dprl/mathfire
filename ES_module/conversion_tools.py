"""
This file provides the conversion tools for TSV to ElasticSearch
"""
import pandas as pd
import numpy


def tsv_to_json(tsv_file_path):
    return pd.read_csv(tsv_file_path, sep="\t").to_json()


def __get_dic(headers, record):
    result_dic = {}
    for i in range(1, len(headers)):
        result_dic[headers[i]] = record[headers[i]]
    return result_dic


def tsv_to_elastic_list(tsv_file_path, index_name):
    """
    Reading TSV file and generating format for ElasticSearch
    """
    print(tsv_file_path)
    errors = 0
    data = pd.read_csv(tsv_file_path, sep="\t", header=0)
    data = data.to_dict("records")
    headers = list(data[0].keys())
    list_data = []
    for record in data:
        # print(record.keys())
        try:
            # print("fine")
            record['formula_vector'] = numpy.asarray(numpy.fromstring(record['formula_vector'][1:-1], sep=' ', dtype=numpy.float32))
            list_data.append(
                {
                    "_index": index_name,
                    "_id": record[headers[0]],
                    "_source": __get_dic(headers, record),
                }
            )
        except:
            errors += 1
            if errors % 10000 == 0:
                print(errors)
            continue
            # print(record.keys())


    return list_data
