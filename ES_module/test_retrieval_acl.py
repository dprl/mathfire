from search import SearchViaES
import sys
from retrieval_tools import CFTInterface


def retrieval(embedding_idex):
    cft_manager = CFTInterface()
    esi = SearchViaES(embedding_idex, cft_manager)
    query = input("insert formula query:\n")
    while query:
        retrieval_result, time_representation_extraction, time_retrieval = esi.retrival(query, 5)
        print("MathML extraction time: " + str(time_representation_extraction))
        print("Retrieval time: " + str(time_retrieval))
        print("Formula Id\tScore")
        for item in retrieval_result:
            print(str(item) + "\t" + str(retrieval_result[item]))
        print("---------------------------------")
        query = input("insert formula query:\n")


def main():
    embedding_index = sys.argv[1]
    retrieval(embedding_index)


if __name__ == '__main__':
    main()
