from ES_module.retrieval_tools import CFTInterface
from search import SearchViaES
import time
import os
import csv

cft_manager = CFTInterface()
esi = SearchViaES(cft_manager)


def read_tsv_file(directory):
    result = {}
    for file in os.listdir(directory):
        with open(directory+"/"+file, 'r', newline='') as result_file:
            csv_reader = csv.reader(result_file, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            next(csv_reader)
            for row in csv_reader:
                formula_id = row[0]
                latex = row[-1]
                result[formula_id] = latex
    return result


if __name__ == '__main__':
    dic_results = read_tsv_file("/home/bm3302/latex_representation_v3")
    query = input("insert formula query:\n")
    while query:
        retrieval_result, time_representation_extraction, time_retrieval = esi.retrival(query, 3000)
        time2 = time.time()
        print("MathML extraction time: " + str(time_representation_extraction))
        print("Retrieval time: " + str(time_retrieval))
        print("rank\tlatex")
        count = 0
        for formula_id in retrieval_result:
            count += 1
            if formula_id not in dic_results:
                print(formula_id)
                continue
            else:
                latex = dic_results[formula_id]
                print(str(count)+"\t"+latex)
            if count == 5:
                break
        print("---------------------------------")
        query = input("insert formula query:\n")