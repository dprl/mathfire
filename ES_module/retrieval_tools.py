import os
import sys
import TangentCFT.tangent_cft_back_end
from TangentCFT.Embedding_Preprocessing.encoder_tuple_level import TupleTokenizationMode
from TangentCFT.tangent_cft_back_end import TangentCFTBackEnd
from latexml_conversion_tool import get_mathmls

conf_path = os.getcwd()
sys.path.append(conf_path)


class CFTInterface:

    def __init__(self,
                encoder_path="TangentCFT/Embedding_Preprocessing/token_ids.tsv",
                config_dir="TangentCFT/Configuration/config/",
                models="TangentCFT/Saved_Models/"):
        self.slt_cft = TangentCFTBackEnd(config_dir + "config_102")
        self.slt_cft.load_model(encoder_path, models + "a3_slt_102")

        self.opt_cft = TangentCFTBackEnd(config_dir + "config_100", read_slt=False, tokenize_number=False)
        self.opt_cft.load_model(encoder_path, models + "a3_opt_100")

        self.slt_type_cft = TangentCFTBackEnd(config_dir + "config_104", tokenize_number=False,
                                              embedding_type=TupleTokenizationMode.Type)
        self.slt_type_cft.load_model(encoder_path, models + "a3_slt_type_104")
        self.opt_type_cft = TangentCFTBackEnd(config_dir + "config_105", read_slt=False, tokenize_number=False,
                                              embedding_type=TupleTokenizationMode.Type)
        self.opt_type_cft.load_model(encoder_path, models + "a3_opt_type_105")

    @staticmethod
    def __extract_representations(formula_latex_string):
        cmml_list, pmml_list = get_mathmls([formula_latex_string])
        return cmml_list[0][1], pmml_list[0][1]

    def get_mathml(self, formula_latex_string):
        cmml, pmml = self.__extract_representations(formula_latex_string)
        return cmml, pmml

    def get_vector(self, is_slt, is_type, representation):
        if is_slt and is_type:
            return self.slt_type_cft.get_vector_representation(representation)
        elif is_slt:
            return self.slt_cft.get_vector_representation(representation)
        elif not is_slt and is_type:
            return self.opt_type_cft.get_vector_representation(representation)
        return self.opt_cft.get_vector_representation(representation)
