import numpy

from ES_module.conversion_tools import tsv_to_elastic_list
from ES_module.elastic_search_interface import ElasticSearchInterFace
from ES_module.retrieval_tools import extract_vectors


class SearchVisESTest:
    def __init__(self):
        self.es_interface = ElasticSearchInterFace()
        self.index_name_slt = "slt_index5"
        self.index_name_slt_type = "slt_type_index5"
        self.index_name_opt = "opt_index5"
        self.index_name_opt_type = "opt_type_index5"

    def generate_vector_index(self):
        self.es_interface.create_index(self.index_name_slt)
        self.es_interface.create_index(self.index_name_slt_type)
        self.es_interface.create_index(self.index_name_opt)
        self.es_interface.create_index(self.index_name_opt_type)

    def load_in_index(self, tsv_file, index_name):
        list_data = tsv_to_elastic_list(tsv_file, index_name)
        self.es_interface.insert_vecs(300, index_name, list_data)

    def retrival(self, latex_formula_query, top_k):
        opt, slt, opt_type, slt_type = extract_vectors(latex_formula_query)
        res_opt = self.es_interface.read_top_k_vec(self.index_name_opt, opt, top_k)
        res_slt = self.es_interface.read_top_k_vec(self.index_name_slt, slt, top_k)
        res_opt_type = self.es_interface.read_top_k_vec(self.index_name_opt_type, opt_type, top_k)
        res_slt_type = self.es_interface.read_top_k_vec(self.index_name_slt_type, slt_type, top_k)

        #### Combine results
        final_results = {}

        return final_results

    def retrieval_modified(self, latex_formula_query, top_k=3):
        slt = list(numpy.ones(300))
        return self.es_interface.read_top_k_vec(self.index_name_slt, slt, top_k)
