#!/bin/bash
################################################################
# install     - Bash script 
# MathFIRE
#
# Authors: B. Mansouri, M. Langsenkamp, R. Zanibbi 
#         (DPRL, RIT, USA, 2022)
################################################################

# Report message
mymsg () {
	echo "    >> $@"
	echo ""
}

# Pass number of arguments sent to outer script and success message to invoke.
testSuccess () {
	if [ $? -ne 0 ]
	then
		echo ""
		echo "** Installation error ** -- please review error messages above."

		# Pass any additional argument, program will not halt.
		if [ $1 -eq 0 ] 
		then
			exit $?
		else
			echo "** Continuing..."
			echo ""
		fi
	else
		# Success.
		mymsg $2
	fi
}

################################################################
# Main Script
################################################################

# Initial message
echo "MathFIRE Installation Script"
echo "-------------------------------------"
if [ $# -gt 0 ]
then
	echo "** Force mode: will continue after errors ** (argument passed)"
fi

##################################
# Data and repsitory downloading
##################################

# LaTeXML
echo ""
echo "[ Downloading LaTeXML 0.8.5]"
if [ -d ./LaTeXML-0.8.5 ]
then
	mymsg "LaTeXML already downloaded."
else
  wget https://math.nist.gov/~BMiller/LaTeXML/releases/LaTeXML-0.8.5.tar.gz
  tar zxvf LaTeXML-0.8.5.tar.gz
  cd LaTeXML-0.8.5
  perl Makefile.PL
  make
  make test
  make install
  cd ..
  rm LaTeXML-0.8.5.tar.gz
fi

# Tangent-S installation
echo ""
echo "[ Downloading Tangent-S ]"
if [ -d ./tangents ]
then
	mymsg "Tangent-s repository already downloaded."
else
  git clone -b behrooz_edits https://gitlab.com/dprl/tangent-s.git
  mv tangent-s tangents
  rm -r TangentS
fi

# Tangent-CFT installation
echo ""
echo "[ Downloading Tangent-CFT ]"
if [ -d ./TangentCFT ]
then
	mymsg "TangentCFT repository already downloaded."
else
  echo "Downloading Tangent-CFT repository"
  git clone -b mathfirecft https://gitlab.com/dprl/TangentCFT.git
  
  echo "Downloading CFT n-gram models (neural network weights)"
  wget https://www.cs.rit.edu/~dprl/data/TangentCFT-ARQMath-3/encoder_arqmath.csv
  wget https://www.cs.rit.edu/~dprl/data/TangentCFT-ARQMath-3/opt.zip
  wget https://www.cs.rit.edu/~dprl/data/TangentCFT-ARQMath-3/opt_type.zip
  wget https://www.cs.rit.edu/~dprl/data/TangentCFT-ARQMath-3/slt.zip
  wget https://www.cs.rit.edu/~dprl/data/TangentCFT-ARQMath-3/slt_type.zip
  
  echo "Unzipping CFT n-gram models"
  unzip opt.zip -d TangentCFT/Saved_Models
  unzip opt_type.zip -d TangentCFT/Saved_Models
  unzip slt.zip -d TangentCFT/Saved_Models
  unzip slt_type.zip -d TangentCFT/Saved_Models

  echo "Removing CFT n-gram models zip files"
  rm opt.zip
  rm opt_type.zip
  rm slt.zip
  rm slt_type.zip
fi

##################################
# Generating tools / environments
##################################

# Set up Conda environment
echo "[ Creating Conda Python Environment (MathFIRE) ]"

CENV=`conda info --envs`
if grep -w "mathfire" <<< "$CENV"
then
	mymsg "Conda mathfire environment already created."
else
	conda create -n mathfire python=3.8.8
	testSuccess $# "Environment (mathfire) created successfully."
  CONDSH=`conda info | grep 'base environment' | awk '{print $4}'`/etc/profile.d/conda.sh
	source $CONDSH
	conda activate mathfire
	pip install -r requirements.txt
	pip install -r TangentCFT/requirements.txt
	conda install -c conda-forge pypandoc
	testSuccess $# "Additional packages installed successfully."
	conda deactivate
fi



##################################
# Finish
##################################

# Report success.
echo "Installation complete!"
echo ""
echo "To test the MathFIRE installation, issue (see README.md for details):"
echo ""
echo "    make example"



