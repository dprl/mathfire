# Author: Behrooz Mansouri (bm3302@rit.edu)
# This class reads TSV files from Extraction Pipeline into python dictionaries
# These TSV file have different information about formula, their location, page and document they are located in
import csv


class ExtractionReader:
    # In TSV files from extraction pipeline each row starts with an identifier showing what that row shows
    document_identifier = "D"
    page_identifier = "P"
    formula_region_identifier = "FR"
    mathml_identifier = "MML"

    def __init__(self):
        pass

    @staticmethod
    def __read_tsv_file(file_path):
        """
        This method reads the TSV file from the Extraction Pipeline. Each row starts with an identifier showing
        what that row contains. The methods returns the document id, its path and a dictionary of pages as {page id:
        dictionary of formulas. The dictionary of formulas are in shape of {formula id, tuple}. Note that the formula id
        is the formula id used in extraction pipeline not indexing. The tuple is (location, mathml). Location is a list
        of 4 numbers, and the mathml is string showing presentation mathml.
        @param file_path: the file path of TSV file created by Extraction pipeline
        @return: document id, document path, dictionary of {page id, dictionary of formulas}
        """
        doc_id = ""
        doc_path = ""
        dic_pages = {}
        dic_current_page = {}
        current_page = -1
        with open(file_path, mode='r') as infile:
            csv_reader = csv.reader(infile, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            for row in csv_reader:
                if row[0] == ExtractionReader.document_identifier:
                    # doc_id = row[1]
                    doc_path = row[2]
                    doc_id = doc_path.split("/")[-1].split(".")[0]
                elif row[0] == ExtractionReader.page_identifier:
                    if len(dic_current_page.keys()) > 0:
                        dic_pages[current_page] = dic_current_page
                    current_page = int(row[1])
                    dic_current_page = {}
                elif row[0] == ExtractionReader.formula_region_identifier:
                    formula_id = int(row[1])
                    location = row[2:]
                    # after reading a formula region, mathml should be found
                    while row[0] != ExtractionReader.mathml_identifier:
                        row = next(csv_reader)
                    MML = row[2]
                    MML = MML.replace("\\n", "\n", MML.count("\\n"))
                    dic_current_page[formula_id] = (location, MML)
            dic_pages[current_page] = dic_current_page
        return doc_id, doc_path, dic_pages

    @staticmethod
    def read_list_files(lst_file_path):
        """
        Takes in a list of TSV file paths (files extracted from Extraction pipeline) and read them.
        It returns a list of [document id, document path] and a dictionary of {document id, dictionary of {page id:
        dictionary of formulas}. The dictionary of formulas are in shape of {formula id, (location, mathml)}.
        @param lst_file_path: list of tsv file paths
        @return: list of [document id, document path] and dictionary of {document id, dictionary of {page id:
        dictionary of formulas}
        """
        lst_document_id_path = []
        dic_document_id_dic_pages = {}
        for file in lst_file_path:
            doc_id, doc_path, dic_pages = ExtractionReader.__read_tsv_file(file)
            lst_document_id_path.append([doc_id, doc_path])
            dic_document_id_dic_pages[doc_id] = dic_pages
        return lst_document_id_path, dic_document_id_dic_pages
