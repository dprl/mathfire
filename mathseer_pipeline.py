#!/usr/bin/env python
# -*- coding:utf-8 -*-

import html
import os
from copy import copy
import subprocess
import sys
from tangents.src.python.math.math_extractor import MathExtractor
from bs4 import BeautifulSoup
from lxml import etree
import xml.etree.ElementTree as ET
import csv
csv.field_size_limit(sys.maxsize)
sys.setrecursionlimit(10000)


SUBPROCESS_TIMEOUT = 10000
Port = 3354
LATEXMLC = [
    'latexmlc',
    '--preload=amsmath',
    '--preload=amsfonts',
    '--pmml',
    '--cmml',
    '--whatsin=fragment',
    '--whatsout=fragment',
    '--format=html5',
    '--port='+str(Port),
    '-',
]

XML_NAMESPACES = {
    'xhtml': 'http://www.w3.org/1999/xhtml',
    'mathml': 'http://www.w3.org/1998/Math/MathML',
    'ntcir-math': 'http://ntcir-math.nii.ac.jp/',
}
ETREE_TOSTRING_PARAMETERS = {
    'xml_declaration': True,
    'encoding': 'UTF-8',
    'with_tail': False,
}


def start_latexml_server():
    """
    Starting the latexml server on port 3990
    :return:
    """
    os.system('latexmls --port=' + str(Port))

def remove_noise(xx):
    tree = etree.XML(str(xx))
    for el in tree.xpath("//*"):
        for attr in el.attrib:
            if attr.startswith("id") or attr.startswith("xref"):
                el.attrib.pop(attr)
    return ET.tostring(tree, encoding='unicode', method='xml')


def latexml(latex_input):
    result = execute(LATEXMLC, latex_input)
    temp = resolve_share_elements(result)
    soup = BeautifulSoup(temp, 'lxml')
    mydivs = soup.find_all("div", {"class": "ltx_para"})
    presentation_result = []
    content_result = []
    for div_tag in mydivs:
        math_tag = div_tag.findAll("math")

        for item in math_tag:
            presentation = MathExtractor.isolate_pmml(str(item))
            presentation = remove_noise(presentation)
            presentation_result.append(presentation)
            content = MathExtractor.isolate_cmml(str(item))
            content = remove_noise(content)
            content_result.append(content)
    return presentation_result, content_result


def resolve_share_elements(xml_tokens):
    xml_document = BeautifulSoup(xml_tokens, 'lxml')
    for math_element in xml_document.find_all('math'):
        for share_element in math_element.find_all('share'):
            if 'href' not in share_element:
                share_element.decompose()
                continue
            assert share_element['href'].startswith('#')
            shared_element = math_element.find(id=share_element['href'][1:])
            if shared_element:
                share_element.replace_with(copy(shared_element))
            else:
                share_element.decompose()
    return str(xml_document)


def execute(command, unicode_input):
    # unicode_input = "$a+b$"
    str_input = unicode_input.encode('utf-8')
    # print(str_input)
    process = subprocess.Popen(
        command,
        shell=False,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.DEVNULL,
    )
    # print(command)
    try:
        str_output = process.communicate(
            str_input,
            timeout=SUBPROCESS_TIMEOUT,
        )[0]
        # print(str_output)
    except subprocess.TimeoutExpired as e:
        process.kill()
        raise e
    unicode_output = str_output.decode('utf-8')
    return unicode_output


def convert_to_dollar(list_latex):
    result = ""
    for latex in list_latex:
        if not latex.startswith("$"):
            result += "$" + latex + "$" + "\n"
        else:
            result += latex + "\n"
    return result


def get_mathmls(latex_list):
    temp = "\n\n"
    temp = temp.join(latex_list)
    present_lsit, content_lsit = latexml(temp)

    for i in range(0, len(content_lsit)):
        soup = BeautifulSoup(content_lsit[i], 'html.parser')
        content_lsit[i] = html.unescape(str(soup))
    for i in range(0, len(present_lsit)):
        soup = BeautifulSoup(present_lsit[i], 'html.parser')
        present_lsit[i] = html.unescape(str(soup))

    all_content = {}
    all_presentaion = {}

    if len(latex_list) == len(present_lsit) and len(latex_list) == len(content_lsit):
        for i in range(len(latex_list)):
            all_content[latex_list[i]] = content_lsit[i]
            all_presentaion[latex_list[i]] = present_lsit[i]
        return all_presentaion, all_content
    if len(latex_list) == 1:
        return {}, {}
    else:
        for i in range(0, len(latex_list)):
            latex_str = latex_list[i][1:-1]
            latex_str = "".join(latex_str.split())
            for j in range(0, len(present_lsit)):
                mathml = present_lsit[j]
                if "alttext=\""+latex_str in mathml:
                    all_content[latex_list[i]] = content_lsit[j]
                    all_presentaion[latex_list[i]] = present_lsit[j]
                    break
        return all_presentaion, all_content
        # pass
    # latex_rows_head = latex_list[:len(latex_list) // 2]
    # latex_rows_tail = latex_list[len(latex_list) // 2:]
    # all_presentaion_head, all_content_head = get_mathmls(latex_rows_head)
    # all_presentaion_tail, all_content_tail = get_mathmls(latex_rows_tail)
    # all_presentaion_head.update(all_presentaion_tail)
    # all_content_head.update(all_content_tail)


