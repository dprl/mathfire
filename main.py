from conversion_pipeline import ConversionPipeline


def main():
    """Sample Run"""
    cp = ConversionPipeline()
    cp.read_extraction_files_and_write_index(["Sample_files/1.tsv", "Sample_files/1.tsv"])


if __name__ == '__main__':
    main()
