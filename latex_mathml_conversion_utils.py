# Author: Behrooz Mansouri (bm3302@rit.edu)
# This class manages the conversion from LaTex to MathMLs and from MathML to LaTex
import html

import pypandoc
from tqdm import tqdm

import latexml_conversion_tool
from tangents.src.python.math.math_extractor import MathExtractor
from tangents.src.python.math.symbol_tree import SymbolTree


def break_list(lst_latex_strings):
    return [lst_latex_strings[x:x+100] for x in range(0, len(lst_latex_strings), 100)]


class ConversionUtils:
    def __init__(self, last_visual_id, visual_ids_slt={}, visual_ids_latex={}):
        self.last_visual_id = last_visual_id
        self.visual_ids_slt = visual_ids_slt
        self.visual_ids_latex = visual_ids_latex

    @staticmethod
    def pmml_to_latex_all(dic_formula_id_pmml):
        """
        Takes in list of Presentation MathML and return their LaTex representations
        @dic_formula_id_pmml : dictionary of formula id: presentation MathML
        :return: list of LaTex string
        """
        lst_pmml_strings = list(dic_formula_id_pmml.values())
        print("number of formulas: " + str(len(lst_pmml_strings)))
        temp = ""
        for i in range(0, len(lst_pmml_strings)):
            lst_pmml_strings[i] = lst_pmml_strings[i].strip()
            temp += "$" + str(i) + "###" + lst_pmml_strings[i]
        output = pypandoc.convert_text(temp, 'latex', format='html')
        items = output.split("\$")[1:]
        results = [""] * len(lst_pmml_strings)
        exceptions = []
        for i in range(0, len(items)):
            parts = items[i].split("\#\#\#")
            try:
                index = int(parts[0])
                latex = parts[1]
                latex = latex[1:-1].strip()
                if i == len(items) - 1:
                    latex = latex[:-1]
                if latex != "":
                    results[index] = latex.strip()
                else:
                    exceptions.append(i)
            except:
                exceptions.append(i)
        return results, exceptions

    @staticmethod
    def pmml_to_latex(dic_formula_id_pmml):
        """
        Takes in list of Presentation MathML and return their LaTex representations
        @dic_formula_id_pmml : dictionary of formula id: presentation MathML
        :return: list of LaTex string
        """
        lst_pmml_strings = list(dic_formula_id_pmml.values())
        print("number of formulas: " + str(len(lst_pmml_strings)))
        chunks = [lst_pmml_strings[x:x + 5000] for x in range(0, len(lst_pmml_strings), 5000)]
        final_result = []
        exceptions = []
        for lst in tqdm(chunks):
            temp_list = lst
            temp = ""
            for i in range(0, len(temp_list)):
                temp_list[i] = temp_list[i].strip()
                temp += "$" + str(i) + "###" + temp_list[i]
            output = pypandoc.convert_text(temp, 'latex', format='html')
            items = output.split("\$")[1:]
            results = [""] * len(temp_list)

            for i in range(0, len(items)):
                parts = items[i].split("\#\#\#")
                try:
                    index = int(parts[0])
                    latex = parts[1]
                    latex = latex[1:-1].strip()
                    if i == len(items) - 1:
                        latex = latex[:-1]
                    if latex != "":
                        results[index] = latex.strip()
                    else:
                        exceptions.append(i)
                except:
                    exceptions.append(i)
            final_result.extend(results)
        return final_result, exceptions

    @staticmethod
    def latex_to_cmml_pmml(lst_latex_strings):
        """
        Takes in a list of latex strings and returns a list of Presentation and a list Content MathMLs
        @param lst_latex_strings:
        @return:
        """
        lst_of_list = break_list(lst_latex_strings)
        final_pmls = []
        final_cmls = []
        for latex_list in tqdm(lst_of_list):
            cmls, pmls = latexml_conversion_tool.get_mathmls(latex_list)
            for i in range(len(pmls)):
                pmls[i] = html.unescape(pmls[i][1])
                pmls[i] = pmls[i].replace("\n", " ")
                cmls[i] = html.unescape(cmls[i][1])
                cmls[i] = cmls[i].replace("\n", " ")
                final_pmls.append(pmls[i])
                final_cmls.append(cmls[i])
        return final_pmls, final_cmls

    def get_visual_id(self, string_rep, is_slt=True):
        """
        Decides the visual id for the current formula if the PresentationMathML (SLT) is passed will
        check the id with SLT dictionary otherwise uses the LaTex dictionary.
        If the formula visual representation does not exist, will add it to the corresponding dictionary and
        update the last visual id and dictionaries.
        @param string_rep: formula representation
        @param is_slt: decides if the representation is slt or not (LaTex)
        @return: visual id of the formula
        """
        if is_slt:
            string_value = self.convert_mathml_slt_string(string_rep)
            if string_value not in self.visual_ids_slt:
                self.visual_ids_slt[string_value] = self.last_visual_id
                self.last_visual_id += 1
            return self.visual_ids_slt[string_value]
        else:
            string_value = "".join(string_rep.split())
            if string_value not in self.visual_ids_latex:
                self.visual_ids_latex[string_value] = self.last_visual_id
                self.last_visual_id += 1
            return self.visual_ids_latex[string_value]

    @staticmethod
    def convert_mathml_slt_string(mathml):
        pmml = MathExtractor.isolate_pmml(mathml)
        return SymbolTree(MathExtractor.convert_to_layoutsymbol(pmml)).tostring()
